package app.main;

import app.ui.Launcher;
import javafx.application.Application;

public class Main {

    public static void main(String[] args) {
        Application.launch(Launcher.class, args);
    }
}
