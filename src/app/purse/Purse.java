package app.purse;

import app.errors.InvalidQuantityException;
import app.errors.PurseOutOfMoneyException;
import app.errors.NonexistentDenominationException;
import app.errors.NotEnoughMoneyException;

import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

public class Purse {
    private TreeMap<Integer,Integer> collector;

    public Purse(){
        collector = new TreeMap<>();
    }

    /**
     * Get the collector
     * @return Collector
     */
    public TreeMap<Integer,Integer> getCollector() {
        return this.collector;
    }

    /**
     * Adds a specific quantity of a coin to the purse
     * @param value Value of coin
     * @param quantity Number of coins
     */
    public void insertCash(Integer value, Integer quantity) throws
            InvalidQuantityException, NonexistentDenominationException {
        if (quantity < 1) {
            throw new InvalidQuantityException(
                    "The specified quantity is invalid.");
        }

        if (!isValidNominalValue(value)) {
            throw new NonexistentDenominationException(
                    "This denomination does not exists");
        }

        if(collector.containsKey(value)) {
            collector.put(value, collector.get(value) + quantity);
        } else {
            collector.put(value, quantity);
        }
    }

    /**
     * Validate the nominal value
     * @param value Nominal value
     * @return true if denomination is valid
     * 5
     * 50
     */
    private boolean isValidNominalValue(Integer value) {
        return  Pattern.compile("[12]0{1,2}|50{0,1}")
                .matcher(value.toString()).matches();
    }

    /**
     * Returns the sum of all money in purse
     * @return Value of all coin
     */
    public Integer getTotalValue(TreeMap<Integer, Integer> collector) {
        Integer value = 0;

        for(Map.Entry<Integer, Integer> entry: collector.entrySet()) {
            value += entry.getKey() * entry.getValue();
        }

        return value;
    }

    /**
     * Check if the collector is empty
     * @return true if collector is empty
     */
    public boolean isCollectorEmpty(TreeMap<Integer,Integer> collector) {
        return collector.isEmpty();
    }

    /**
     * Removes all money from the collector
     */
    public void clearCollector() {
        this.collector.clear();
    }

    private Integer round(Integer value) {
        if( value % 5 == 0 ) {
            return value;
        }

        return ( value % 5 > 2 ) ? (value + ( 5 - value%5 )) : (value - value % 5);
    }

    /**
     * Check if the parameter map's lowest key
     * is bigger than the remaining amount of money.
     * @param collector Collector with the remaining money
     * @param amount Remaining amount of money
     * @return true if smallest key is bigger than the remaining amount
     */
    private boolean isSmallestKeyBiggerThanAmount(
            TreeMap<Integer,Integer> collector, Integer amount) {
        return amount.compareTo(collector.firstKey()) < 0;
    }

    public TreeMap<Integer, Integer> withdrawMoney(Integer amount)
            throws NotEnoughMoneyException, PurseOutOfMoneyException, InvalidQuantityException {
        TreeMap<Integer, Integer> tempCollector = new TreeMap<>();
        TreeMap<Integer, Integer> withdrawal = new TreeMap<>();

        if (amount <= 0) {
            throw new InvalidQuantityException(
                    "Please enter a valid number");
        }

        //Round the requested amount of money
        amount = round(amount);

        //Withdrawal exceeds the money in collector
        if(amount.compareTo(getTotalValue(this.collector)) > 0 ) {
            throw new NotEnoughMoneyException(
                    "The withdrawal amount exceeds the amount of money in the purse.");
        }

        //Copy collector
        tempCollector.putAll(this.collector);

        //If smaller
        while( amount > 0 && !isCollectorEmpty(tempCollector)
                && !isSmallestKeyBiggerThanAmount(tempCollector, amount) )  {

            //Biggest nominal value greater than withdrawal amount
            if(tempCollector.lastKey() > amount) {
                tempCollector.remove(tempCollector.lastKey());
            } else {
                //Decrease remaining amount
                amount = amount - tempCollector.lastKey();

                //Decrease quantity of nominal value by 1
                tempCollector.put(tempCollector.lastKey(),
                        tempCollector.get(tempCollector.lastKey()) - 1 );

                //Add nominal value to withdrawal
                if (withdrawal.containsKey(tempCollector.lastKey())) {
                    withdrawal.put(tempCollector.lastKey(),
                            withdrawal.get(tempCollector.lastKey()) + 1 );
                } else {
                    withdrawal.put(tempCollector.lastKey(), 1);
                }

                //That was the last piece of the note
                if (tempCollector.get(tempCollector.lastKey()) == 0) {
                    tempCollector.remove(tempCollector.lastKey());
                }
            }
        }

        //Out of coins
        if(amount > 0) {
            throw new PurseOutOfMoneyException(
                    "Request cannot be performed. There's not enough money in the purse.");
        }

        //Remove notes with zero quantity
        organizeCollector(withdrawal);

        return withdrawal;
    }

    /**
     * Remove all notes and coint with 0 quantity.
     */
    private void organizeCollector(TreeMap<Integer, Integer> withdrawal) {

        //Substract withdrawal
        for (Map.Entry<Integer, Integer> entry: withdrawal.entrySet()) {
            collector.put(entry.getKey(),
                    collector.get(entry.getKey()) - withdrawal.get(entry.getKey()) );
        }

        //Remove zero values
        this.collector.entrySet().removeIf(entry -> entry.getValue().equals(0));
    }


}