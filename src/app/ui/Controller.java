package app.ui;

import app.errors.InvalidQuantityException;
import app.errors.PurseOutOfMoneyException;
import app.errors.NonexistentDenominationException;
import app.errors.NotEnoughMoneyException;
import app.purse.Purse;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.TreeMap;

public class Controller {

    @FXML
    private ListView<String> lwDepositedMoney;

    @FXML
    private TextArea taOutput;

    @FXML
    private ComboBox<Integer> cbDenomination;

    @FXML
    private TextField tfQuantity;

    @FXML
    private Button btnDeposit;

    @FXML
    private TextField tfWithdrawAmount;

    @FXML
    private Button btnWithdraw;

    @FXML
    private Button btnReset;

    @FXML
    private Button btnClearOutput;

    @FXML
    private Label lblTotal;

    private Purse purse;

    public Controller() {
        purse = new Purse();
    }

    @FXML
    public void initialize() {
        initDenomination();

        btnDeposit.setOnAction(event -> deposit());
        btnWithdraw.setOnAction(event -> withdraw());
        btnClearOutput.setOnAction(event -> clearOutput());
        btnReset.setOnAction(event -> reset());
    }

    private void initDenomination() {
        ObservableList<Integer> denomination = FXCollections.observableArrayList();
        denomination.add(5);
        denomination.add(10);
        denomination.add(20);
        denomination.add(50);
        denomination.add(100);
        denomination.add(200);

        cbDenomination.setItems(denomination);
    }

    /**
     * Deposit money
     */
    private void deposit() {
        try {
          this.purse.insertCash(
                  cbDenomination.getValue(),
                  Integer.parseInt(tfQuantity.getText()) );

          //Refresh collector
          refreshList();

          //Clear
          tfQuantity.clear();
        } catch (NumberFormatException e) {
            setOutput("Please enter a valid quantity. Quantity should be bigger than 0.");
        } catch (InvalidQuantityException | NonexistentDenominationException e) {
            setOutput(e.getMessage());
        }
    }

    /**
     * Refresh the content list based on the
     * content of the collector
     */
    private void refreshList() {
        if( !this.purse.isCollectorEmpty(this.purse.getCollector())) {
            ObservableList<String> list = FXCollections.observableArrayList();

            //User info
            this.purse.getCollector().forEach((key, value) -> list.add(key + " x " + value));

            //Money in collector
            lwDepositedMoney.setItems(list);

            //Set total collector value
            lblTotal.setText(this.purse.getTotalValue(
                    this.purse.getCollector())
                    .toString() );
        } else {
            lwDepositedMoney.setItems(null);
            lblTotal.setText("Empty");
        }

    }

    /**
     * Set the console output
     * @param str String to be displayed
     */
    private void setOutput(String str) {
        taOutput.setText(str);
    }

    /**
     * Clear the console
     */
    private void clearOutput() {
        taOutput.clear();
    }

    /**
     * Withdraw money from the collector
     */
    private void withdraw() {
        try {
            TreeMap<Integer, Integer> withdrawal = this.purse
                    .withdrawMoney(Integer.parseInt(
                            tfWithdrawAmount.getText()));

            //information for user
            printWithdrawal(withdrawal);

            //refresh collector
            refreshList();

            //Clear textfield
            tfWithdrawAmount.clear();
        } catch (NotEnoughMoneyException | PurseOutOfMoneyException
                | InvalidQuantityException e) {
            clearOutput();
            setOutput(e.getMessage());
        } catch (NumberFormatException e) {
            clearOutput();
            setOutput("Please enter a valid number to the textfield");
        } finally {
            refreshList();
        }
    }

    /**
     * Print the user's cash after a successful withdrawal
     * @param withdrawal Map that contains the cash
     */
    private void printWithdrawal(TreeMap<Integer, Integer> withdrawal) {
        clearOutput();

        withdrawal.forEach( (k,v) ->
                taOutput.appendText(k + "Ft x " + v + " db\n")
        );
    }

    /**
     * Reset the collector
     */
    private void reset() {
        this.purse.clearCollector();
        this.refreshList();
        this.clearOutput();
    }

}
