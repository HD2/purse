package app.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Launcher extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass()
                .getResource("ui.fxml"));

        primaryStage.setTitle("Purse app");

        primaryStage.setScene(
                new Scene(root, 600, 500));

        //set min width and height
        primaryStage.setMinWidth(600);
        primaryStage.setMinHeight(500);

        primaryStage.show();
    }
}
