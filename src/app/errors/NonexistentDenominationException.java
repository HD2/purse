package app.errors;

public class NonexistentDenominationException extends Exception {

    public NonexistentDenominationException(String message) {
        super(message);
    }

}
