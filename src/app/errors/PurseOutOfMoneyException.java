package app.errors;

public class PurseOutOfMoneyException extends Exception{
    public PurseOutOfMoneyException(String message) {
        super(message);
    }
}
