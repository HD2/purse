package app.purse;

import app.errors.InvalidQuantityException;
import app.errors.PurseOutOfMoneyException;
import app.errors.NonexistentDenominationException;
import app.errors.NotEnoughMoneyException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class PurseTest {
    private Purse purse;

    @BeforeEach
    void setUp() {
        purse = new Purse();
    }

    @Test
    void testInsertCashWithNegativeQuantity(){
        assertThrows(InvalidQuantityException.class, () ->
                purse.insertCash(5,-3));
    }

    @Test
    void testInsertCashWithNotAvailableDenomination() {
        assertThrows(NonexistentDenominationException.class, () ->
                purse.insertCash(2,5));
    }

    @Test
    void testCashInsertion() throws
            InvalidQuantityException, NonexistentDenominationException {
        purse.insertCash(5,10);
        purse.insertCash(10,10);
        purse.insertCash(100,2);

        assertEquals(350,
                purse.getTotalValue(
                        purse.getCollector()).intValue() );

    }

    @Test
    void testEmptyCollector() {
        purse.clearCollector();

        Assertions.assertTrue(purse
                .isCollectorEmpty(purse.getCollector()) );
    }

    @Test
    void testSuccessfulWithdraw() throws
            InvalidQuantityException, NonexistentDenominationException,
            PurseOutOfMoneyException, NotEnoughMoneyException {
        purse.insertCash(5000,2);
        purse.insertCash(1000,3);
        purse.insertCash(500,4);

        TreeMap<Integer, Integer> result =
                purse.withdrawMoney(10000);

        assertEquals(10000,
                purse.getTotalValue(result).intValue());
    }

    @Test
    void testPurseOutOfMoney() throws
            InvalidQuantityException, NonexistentDenominationException {
        purse.insertCash(5000,2);

        Assertions.assertThrows(PurseOutOfMoneyException.class, () ->
                purse.withdrawMoney(5500) );
    }

    @Test
    void invalidQuantityException() throws
            InvalidQuantityException, NonexistentDenominationException {
        purse.insertCash(2000,3);

        Assertions.assertThrows(InvalidQuantityException.class, () ->
            purse.withdrawMoney(0));
    }

    @Test
    void testNotEnoughMoneyInPurse() throws
            InvalidQuantityException, NonexistentDenominationException {
        purse.insertCash(5000,2);

        Assertions.assertThrows(NotEnoughMoneyException.class, () ->
                purse.withdrawMoney(100000) );

    }
}